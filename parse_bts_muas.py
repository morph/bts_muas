#!/usr/bin/python2.6

from __future__ import with_statement
import sys
import email
import time
from collections import defaultdict
import os
import fnmatch
import signal
import re

# trap Ctrl+C and emit partial results
def print_partial_results(signal, frame):
    print_results(reportbugs, muas)
    sys.exit(0)

signal.signal(signal.SIGINT, print_partial_results)

def parse_bts_message(filename, message, reportbugs, muas):
    msg = email.message_from_string(tmp)
    date = email.utils.parsedate(msg.get('Date'))
    if not date:
        return
    try:
        d = time.strftime('%Y-%m-%d', date)
    except:
        print "Error decoding date (%s) for %s" % (date, filename)
        return
    xmailer = msg.get('X-Mailer')
    useragent = msg.get('User-Agent')
    if xmailer:
        xmailer = xmailer.lower()
        xmailerl = xmailer.split()
        # special case for reportbug only
        if xmailerl[0] == 'reportbug':
            reportbugs[d][xmailerl[1]] += 1
            muas[d][xmailerl[0]] += 1
        elif xmailerl[0] in ['apple', 'balsa', 'becky', 'beefmail', 'bug', 'bug-buddy', 'bug-ja', 'caramail', 'cmail', 'claws', 'dmailweb', 'dpkg-mail-changes', 'elm', 'emacs', 'evolution', 'exmh', 'forte', 'git-send-email', 'git-tag-pending-bugs', 'gnus', 'gnumail', 'kmail', 'lotus', 'mail', 'mew', 'mh-e', 'mime-tools', 'mozilla', 'mutt', 'mutt-ng', 'neomail', 'netcat', 'pegasus', 'pms', 'squirrelmail', 'svn-closes-hook', 'svn-tag-pending-bugs', 'sylpheed', 'sylpheed-claws', 'trac', 'uebimiau', 'vm', 'www-mail', 'xfmail', 'ximian', 'yamail', 'zimbra']:
            muas[d][xmailerl[0]] += 1
        elif len(xmailerl) >= 2 and xmailerl[1] in ['gnus', 'bat!', 'interoffice']:
            muas[d][xmailerl[1]] += 1
        elif len(xmailerl) >= 3 and xmailerl[2] in ['eudora',]:
            muas[d][xmailerl[2]] += 1
        elif xmailerl[0].startswith('evolution'):
            muas[d]['evolution'] += 1
        elif xmailerl[0].startswith('yahoo'):
            muas[d]['yahoo'] += 1
        elif xmailerl[0].startswith('squirrelmail'):
            muas[d]['squirrelmail'] += 1
        elif 'outlook' in xmailer:
            muas[d]['outlook'] += 1
        else:
            muas[d]['other'] += 1
            print "unrecognized X-Mailer: %s" % xmailer
    elif useragent:
        useragent = useragent.lower()
        useragentl = re.split(r'\\|/|-| ', useragent)
        #for sep in ['\\', '/', '-']:
        #    useragentl = useragentl.replace(sep, ' ')
        #useragentl = useragentl.split()
        if useragentl[0] in ['alpine', 'devscripts', 'emiko', 'fakemail', 'gemini', 'icedove', 'gnus', 'kmail', 'madmutt', 'mozilla', 'mutt', 'notmuch', 'opera', 'roundcube', 'semi', 'squirrelmail', 'sup', 't-gnus', 'thunderbird', 'tin', 'wanderlust', 'wemi']:
            muas[d][useragentl[0]] += 1
        elif len(useragentl) >= 2 and useragentl[1] in ['thunderbird',]:
            muas[d][useragentl[1]] += 1
        else:
            muas[d]['other'] += 1
            print "unrecognized User-Agent: %s" % useragent
    # think about how to determin a file has no matching header
    #else:
    #    print "%s: unknown header" % filename

def print_results(reportbugs, muas):
    print "writing reportbugs.yaml"
    with open('reportbugs.yaml', 'w') as f:
        f.write('reportbugs\n')
        for key in sorted(reportbugs.keys()):
            f.write('    %s\n' % key)
            for kkey in sorted(reportbugs[key].keys()):
                f.write('        %s: %s\n' % (kkey, reportbugs[key][kkey]))
    print "writing muas.yaml"
    with open('muas.yaml', 'w') as f:
        f.write('muas\n')
        for key in sorted(muas.keys()):
            f.write('    %s\n' % key)
            for kkey in sorted(muas[key].keys()):
                f.write('        %s: %s\n' % (kkey, muas[key][kkey]))

reportbugs = defaultdict(lambda : defaultdict(int))
muas = defaultdict(lambda : defaultdict(int))

tmp = ''
is_incomingrecv = False

for directory in sys.argv[1:]:
    for root, dirnames, filenames in os.walk(directory):
        for fname in fnmatch.filter(filenames, '*.log'):
            filename = os.path.join(root, fname)
            with open(filename) as f:
                for line in f.readlines():
                    if is_incomingrecv:
                        if line == '\n':
                            is_incomingrecv = False
                            parse_bts_message(filename, tmp, reportbugs, muas)
                            tmp = ''
                        else:
                            tmp += line

                    if line == '\n':
                        is_incomingrecv = True


print_results(reportbugs, muas)
